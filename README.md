------------------------------------------------------------------------------
Programa wci Statistic analysis for texts under  GPL V.3 licence
Copyright (C) 2020 Juan Ruiz Segura juan@segura.pro

This program is free software: you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License 
along with this program. If not, see <https://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

wci - Word Count Improved
===========================================
Version 3.0.2
Release date 8-february-2021.
Autor: Juan Ruiz Segura: juan@segura.pro
Licence GPL V 3.0

Input / Output
-----------------------------
- It creates an statistical analysis of texts an a dicctionary of every word in a file. 
- Output fields for each word in the dicctionary:
  Word, Length, Count, Probability, (Tab separation).
- Default input STDIN 
- Default output STDOUT
- It shows by default an alphabetic list (dicctionary) of the text strings,
  by decreasing order, and a statistical summary.
- Max length for a word  32 characters.
- Max length for a string 320 characters.
    
Sinopsys
----------------
`wci [OPCIONES] [ARCHIVO1] [ARCHIVO2]...`

- The program  (wci) and the studed file must be in the same folder. 
- If you downloaded the program, you must be sure to have execution permission 
  In the wci progra, write this command:  `juanma@mipc$ chmod a+x wci`

Options
------------------------
-c, --count 
    Print the words list by decreased count instead the alphabetical order (see -r).

-d, --stad-word-omit
    Omit data and statistic of every word in the list (shown by default).

-e, -stad-summ-omit
    Omit the final statistic (shown by default).

-f YES|NO, --files-comp YES|NO 
    compares ARCHIVO1 with the other files ARCHIVO2... creating an alfabetical list
    of everey word found in the others that does YES|NOT exist in ARCHIVO1.

-h, --help
    Show this help.

-l, --list-omit
    Omit the words list (shown by default). 

-n NUM, --number NUM
    Max numer of words to print by decreasing order. 

-r, --reverse
    Reversal order for the words count (used with -c). 

-s, string
    Draw strings instead words (by default read just alphabetic characters).

-u, --url
    Create two text files. One for the found emails (wci-url-email.txt) and the
    other for the potential urls (wci-url.txt). 
    In case of existing files, the new records are added to the end of the file.

-v, --verbose
    Shows every filed word

-V, --version
    Show the current version

Utility
-------------------------------------
- Create diccionaries of words or strings.

- Statistic analyis of texts. 

- Foreing languages teaching: I designed this program to create dicctionaries for teaching 
  Spanish Language in a gradual system.  My first idea was to prepare texts with only the studed words.

- Statistic study of different languages. I have found some studies but are very old and
  this program permit create  in seconds those studies which lasted years for linguists.

- Cryptoanalysis for thematic dicctionaries, according the field of study:
  medical, politic, militar...).

- Creation of dicctionaries of strings (-s option), keywords and  url/email.

Compiling
------------------------------------------
The compiled file (wci) given have been done in Debian 64 bits.
To compile, you just need to include the files in the same folder an execute "make".
If every is OK, make will create the executable wci. 

Examples
----------------
`wci < archivo.txt`   Read archivo.txt from  STDIN and show the alphabetical order an statistical summary.

`wci quijote.txt > dic.txt` Create a dicctionary of quijote.txt and save it in dic.txt.

`wci -n 20  archivo.txt`    Shows the 20 first strings, by alphabetical order.

`wci -n 20 -c archivo.txt`  Shows the 20 most frequent strings in archivo.txt.

`wci -u archivo.txt`        Additionally, creata files with the url(potential) and email founds.

Releases history
------------------------------
- wci V 1.0.0 (2020-12-05) Juan Ruiz Segura juan@segura.pro/ Initial launch
- wci V 1.1.0 (2020-12-08) Juan Ruiz Segura juan@segura.pro/ Counting errors
- wci V 2.0.0 (2021-01-24) Juan Ruiz Segura juan@segura.pro/ String an URL analyisis
- wci V 2.1.0 (2021-01-31) Juan Ruiz Segura juan@segura.pro/ Multiple files, url-email lists
- wci v 3.0.0 (2021-02-04) Juan Ruiz Segura juan@segura.pro/ Compare files with list of words [does|not] exists in the first.
- wci v 3.0.1 (2021-02-04) Juan Ruiz Segura juan@segura.pro/ Option omit statistic each word
- wci v 3.0.2 (2021-02-08) Juan Ruiz Segura juan@segura.pro/ Long options, style GNU

