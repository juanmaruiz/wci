/* ------------------------------------------------------------------------------
// Programa wci Análisis estadístico de textos bajo licencia GPL V.3
//  Copyright (C) 2020 Juan Ruiz Segura juan@segura.pro

This program is free software: you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License 
along with this program. If not, see <https://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------*/ 

#include "wci.h"

// ========================================================
int main(int argc, char **argv){
    FILE *arch;
    int (*parser_pal_cad)(FILE *); //Puntero a la función que cargará los archivos

    // Variables generales 
    caracteres = 0;  // Caracteres alfabéticos del archio
    lineas= 0;       // Líneas del archivo
    emails= 0;       // emails del archivo
    lista_palab=NULL;
    lista_palab_1=NULL;
    start = time(NULL); 

    setlocale(LC_CTYPE, "C.UTF-8");
    lee_opciones(argc, argv);
    
    if (urls){ // ---------- si se piden urls prepara los archivos
        archemail = fopen(EMAILFILE, "a");
        archurl = fopen(URLFILE, "a");
    }
  
    //----------------selección función parser
    parser_pal_cad =  strings? parser_cadenas: parser_palabras;

    //------------------ Abre archivos
    if  (optind == argc){ // si solo hay opciones listados de STDIN
        arch = stdin;
        parser_pal_cad(arch);
        lista_palab = lista_ini;
    }else if(file_comp){ // comparar archivo1 con los otros
        archivo_1 = argv[optind]; 
        arch =  fopen(archivo_1, "r");// parser del primer archivo
        if (arch == NULL){
            wprintf(L"Error abriendo archvio %s\n", argv[optind]);
            exit(BADFILE);
        }
        parser_pal_cad(arch); 
        fclose (arch);

        // Guardar-Reset variables generales  para volver a contar
        lista_palab=NULL; // vaciamos la lista para que se genere otra nueva
        lista_palab_1 = lista_ini;  // guardo lista del primer archivo
        palabras_leidas_1 = palabras_leidas;
        palabras_leidas=0;
        palabras_archivadas_1 = palabras_archivadas;
        palabras_archivadas= 0;
        caracteres_1 = caracteres;
        caracteres = 0;
        suma_1 = suma;
        suma = 0;
        lineas_1 = lineas;
        lineas = 0;
        for (int i = ++optind; i < argc; i++){ // analizamos resto archivos
            arch =  fopen(argv[i], "r");
            parser_pal_cad(arch); 
            fclose (arch);
            lista_palab = lista_ini;
            } 
    }else{
        for (int i = optind; i < argc; i++){ // Listado todos archivos
            arch =  fopen(argv[i], "r");
            parser_pal_cad(arch); 
            fclose (arch);
            lista_palab = lista_ini;
        }
    }
    if (palabras_archivadas == 0){
        wprintf(L"Archivos vacios\n");
        exit (VACIO);
    }
    if(file_comp) 
        compara_listas(lista_palab_1, lista_palab);

    imprime_resultados();
    return error;
}


// Impresión de resultados
// ---------------------------------------------------------
void imprime_resultados(void){

    if (!file_comp){
        if(conta == 1)
            ordena_lista_conteo(lista_palab); 
        if(listado == 1)
            print_lista_palab(lista_palab);  
    }
    if(stad_summ == 1) {   // estadística final
        end = time(NULL);
        print_resumen(); 
    }
}

// Parser de cadenas
// ----------------------------------------------------------
int parser_cadenas(FILE *arch){
    wchar_t car; 
    wchar_t  palabra_tmp[MAXCAD];

    car = EOF; 

    reset_cadena(); // resetea variables internas de cada cadena
    do{ // Leemos caracter a caracter para formar palabras
        caracteres++;
        if(long_palabra > MAXCAD) {
            error = TRUNKCAD;
            fwprintf (stderr, L"Truncada cadena, longitud > %d. "
                    "Desbordamiento caracter %d línea %d\n",MAXCAD, caracteres, lineas); 
            car = L'\n';
        }else        
            car = fgetwc(arch);  // Leemos caracter a caracter 

        if (car == L'\n') lineas++;     // contador líneas 
        switch (estado){
            case INICIO:
                if(esalfabeto(car)){
                    estado = URL; 
                    palabra_tmp[long_palabra++]= car; 
                }else if(islocalpart(car)){
                    estado = LOCAL; 
                    palabra_tmp[long_palabra++]= car; 
                }else if(iswdigit(car)){
                    estado = NUMERO;
                    palabra_tmp[long_palabra++]= car; 
                }else if (iswgraph(car)){
                    estado = CADENA; 
                    palabra_tmp[long_palabra++]= car; 
                }
                break;
            case NUMERO:
                if (iswdigit(car)){
                    palabra_tmp[long_palabra++]= car; 
                    lastpoint = 0;
                }else if(esalfabeto(car)){
                    palabra_tmp[long_palabra++]= car; 
                    lastpoint = 0;
                    estado = URL;
                }else if(islocalpart(car)){
                    palabra_tmp[long_palabra++]= car; 
                    lastpoint = 0;
                    estado = LOCAL;
                }else if (car == '.'){
                    palabra_tmp[long_palabra++]= car; 
                    if (lastpoint)
                        estado = CADENA;
                    else{
                        lastpoint++;
                        if (++puntos > 2)
                        estado = URL; 
                       }
                }else if (car == ','){
                    palabra_tmp[long_palabra++]= car; 
                    if (++comas > 2)
                        estado = CADENA;
                }else{
                    palabra_tmp[long_palabra] = L'\0'; 
                    archiva_palabra(palabra_tmp, long_palabra); 
                    reset_cadena();
                }
                break;
            case CADENA:
                //0x0300 <= car <= 0x036F diacríticos son wgraph
                if(iswgraph(car))
                    palabra_tmp[long_palabra++]= car; 
                else{
                    palabra_tmp[long_palabra] = L'\0'; 
                    archiva_palabra(palabra_tmp, long_palabra); 
                    reset_cadena();
                }
                break;
            case URL:
                if (esalfabeto(car) || iswdigit(car) || (car== '-')){
                    palabra_tmp[long_palabra++]= car; 
                    lastpoint= 0; 
                }else if(islocalpart(car)){
                    palabra_tmp[long_palabra++]= car; 
                    lastpoint= 0; 
                    estado = LOCAL;
                }else if(car == '.'){ 
                    palabra_tmp[long_palabra++]= car; 
                    if (lastpoint)      // control punto doble
                        estado = CADENA;
                    else
                        lastpoint= 1; 
                }else if(car == '@'){
                    palabra_tmp[long_palabra++]= car; 
                    if (lastpoint) // contol punto final de localpart
                        estado = CADENA;
                    else {
                        estado = EMAIL;
                        lastpoint = 0;
                    }
                }else if(iswgraph(car)){
                    palabra_tmp[long_palabra++]= car; 
                    estado = CADENA;
                }else{
                    palabra_tmp[long_palabra] = L'\0'; 
                    archiva_palabra(palabra_tmp, long_palabra); 
                    reset_cadena();
                    if(urls)
                        guardaurl(palabra_tmp);
                }
                break;
            case LOCAL:
                if(esalfabeto(car) || iswdigit(car) || islocalpart(car)){
                    palabra_tmp[long_palabra++]= car; 
                    lastpoint = 0;
                }else if(car == '.'){ 
                    palabra_tmp[long_palabra++]= car; 
                    if (lastpoint)      // control punto doble
                        estado = CADENA;
                    else
                        lastpoint= 1; 
                }else if(car == '@'){
                    palabra_tmp[long_palabra++]= car; 
                    if (lastpoint) // contol punto final de localpart
                        estado = CADENA;
                    else {
                        estado = EMAIL;
                        lastpoint = 0;
                    }
                }else if(iswgraph(car)){
                    palabra_tmp[long_palabra++]= car; 
                    estado = CADENA;
                }else{
                    palabra_tmp[long_palabra] = L'\0'; 
                    archiva_palabra(palabra_tmp, long_palabra); 
                    reset_cadena(); 
                }
                break;
            case EMAIL:
                if(esalfabeto(car) || iswdigit(car)){
                    palabra_tmp[long_palabra++]= car; 
                    if (++longdnslabel > MAXDNSLABEL) // 
                        estado = CADENA;
                    else{
                        lastpoint = 0;
                        lasthyphen = 0;
                    }
                }else if(car == '-' ){
                    palabra_tmp[long_palabra++]= car; 
                    if (longdnslabel++ == 0) // 
                        estado = CADENA;
                    else{
                        lastpoint = 0;
                        lasthyphen = 1;
                    }
                }else if (car == '.'){ 
                    palabra_tmp[long_palabra++]= car; 
                    if (lastpoint  ||   // punto doble
                        lasthyphen ||    // último guión etiqueta
                        (longdnslabel == 0))
                        estado = CADENA;          
                    else{
                        longdnslabel = 0;
                        lastpoint= 1; 
                        lasthyphen = 0;
                    }
                }else if (iswgraph(car)){
                    palabra_tmp[long_palabra++]= car; 
                    estado = CADENA;
                }else{
                    palabra_tmp[long_palabra] = L'\0'; 
                    if(!lasthyphen){
                        emails++;
                        if(urls)
                            guardaemail(palabra_tmp);
                    }
                    archiva_palabra(palabra_tmp, long_palabra); 
                    reset_cadena();
                }
                break;
            default:
                break;
        }
    }while(car != EOF);
    caracteres--; // Resta EOF que se sumó al principio 
    return 0;
}

//- Carga palabras puras
//--------------------------------------------------
int parser_palabras(FILE *arch){
    wchar_t car; 
    wchar_t  palabra_tmp[MAXPAL];

    // --------- Variables generales
    caracteres = 0;  // Caracteres alfabéticos del archio
    lineas= 0;       // Líneas del archivo
    car = EOF; 
    start = time(NULL); 
    
    reset_cadena();// resetea variables internas de cada cadena
    do{// Leemos caracter a caracter para formar palabras
        if(long_palabra > MAXPAL) {
            error = TRUNKPAL;
            fwprintf (stderr, L"Truncada palabra, longitud > %d. "
                    "Desbordamiento caracter %d línea %d\n",MAXPAL, caracteres, lineas); 
            car = L'\n';
        }else        
            car = fgetwc(arch);  // Leemos caracter a caracter 

        caracteres++;
        if (car == L'\n') lineas++;     // contador líneas 
        switch (estado){
            case INICIO:
                if(iswalpha(car) || (car >=0x0300 && car <= 0x036F )){
                    estado = PALABRA;
                    palabra_tmp[long_palabra++]= car; 
                }
                break;
            case PALABRA:
                if(iswalpha(car) || (car >=0x0300 && car <= 0x036F ))
                    palabra_tmp[long_palabra++]= car; 
                else { // No caracter palabra
                    palabra_tmp[long_palabra] = L'\0'; 
                    archiva_palabra(palabra_tmp, long_palabra); 
                    reset_cadena();
                }
                break;
           default:
                break;
        }
    }while(car != EOF);
    caracteres--; // Resta EOF que se sumó al principio 
    return 0;
}

// Resetea el contador de partes de sintaxis
// -----------------------------------------------------
void reset_cadena (){
    long_palabra = 0; 
    puntos = 0; // controla num de puntos en números
    comas = 0;
    longdnslabel = 0;
    lastpoint = 0;// último caracter viso es punto
    lasthyphen = 0;// último caracter viso un guión
    estado = INICIO;
}


// Busca la palabra en una lista
// archivo alfabetic, devuelve su orden
// -------------------------------------------------------
int archiva_palabra(wchar_t *palabra, int long_palabra){
    int i = 0;
    struct stc_pal stc_tmp, *stc_pal_new;

    palabras_leidas++;
    if(lista_palab == NULL){ // Lista todavía vacía
        lista_palab = guarda_pal_nueva(palabra, long_palabra); 
        lista_ini = lista_palab;
        return 1; 
    } 
    lista_palab = lista_ini;  // reincia el puntero para recorrrer lista
    for (i = 0; i < palabras_archivadas; i++) { // empieza a recorrer la lista
        if( wcscmp(palabra, lista_palab->txt ) > 0 ){  // todavía no hemos llegado 
            if (lista_palab->sig != NULL){   // No final continua
                lista_palab = lista_palab->sig; 
                continue;
            }else{ // final de lista (NULL) archivo palabra nueva
                lista_palab->sig = guarda_pal_nueva(palabra, long_palabra);
                break;
            }
        }else if(wcscmp(palabra, lista_palab->txt ) < 0){ // Nos hemos pasado, hay que archivarla
            // en vez de incrustar la nueva celda intercambiamos el contenido para no
            // necesitar la célula anterior en la lista
                stc_pal_new = guarda_pal_nueva(palabra, long_palabra);
                stc_tmp.txt = lista_palab->txt;
                stc_tmp.cont = lista_palab->cont;
                stc_tmp.sig = lista_palab->sig;

                lista_palab->txt = stc_pal_new->txt;    // o palabra
                lista_palab->cont = stc_pal_new->cont;  
                lista_palab->sig = stc_pal_new;  

                stc_pal_new->txt  = stc_tmp.txt;     
                stc_pal_new->cont = stc_tmp.cont;   
                stc_pal_new->sig  = stc_tmp.sig;  
                break;
        }else{ // encontrada palabra incrementa contad
            lista_palab->cont++;
            break;
        }
    } 
    return i;
}

// Guarda en mem la palabra entregada y devuelve su puntero
// -----------------------------------------------------------------
struct stc_pal *guarda_pal_nueva(wchar_t *palabra, int lng_palabra){
    struct stc_pal *p_tmp;

    p_tmp = (struct stc_pal *) malloc(sizeof (struct stc_pal));
    if (p_tmp == NULL){
        fprintf(stderr, "Memoria error\n");
         exit (ERRMEM);
    }
    p_tmp->sig = NULL; 
    p_tmp->cont = 1;
    p_tmp->txt = (wchar_t *) calloc(lng_palabra+1, sizeof (wchar_t));  //  mem para txt 
    wcscpy (p_tmp->txt, palabra);  // guarda txt
    suma += lng_palabra;

    palabras_archivadas++;
    if (verbose)
        wprintf(L"%ls\r", palabra);
    return (p_tmp);
}

// Imprime todas las palabras orden de lista
// ------------------------------------------------------
void print_lista_palab(struct stc_pal *lista){
    float probab;
    int numpal = 0; 

    do { 
        probab =((float)lista->cont) / (float)palabras_leidas ;
        if(stad_word) 
            wprintf (L"%ls\t%d\t%d\t%.3f\n", lista->txt, wcslen(lista->txt), lista->cont,probab); 
        else
            wprintf (L"%ls\n", lista->txt); 
        lista= lista->sig; 
    }while (lista != NULL && (++numpal < numax || numax == 0));
} 

// Compara dos palabras en el orden deseado
//----------------------------------------------------------
int compara (int num_left, int num_right, int rev){
    if (rev)
        return(num_left > num_right);
    else
        return(num_left < num_right);
}

//  Ordena la lista obtenida por conteo de palabras
//  --------------------------------------------------------------- 
void ordena_lista_conteo(struct stc_pal *lista){ 
    struct stc_pal 
        stc_tmp, 
        *stc_burbuja,
        *stc_ini;

    stc_ini = lista;
    while (lista->sig != NULL) { // empieza a recorrer la lista (burbuja)
        stc_burbuja = lista->sig;
        do {
            if( compara(lista->cont , stc_burbuja->cont, rever) ){  // orden incorrecto cambia
                stc_tmp.txt = lista->txt;
                stc_tmp.cont = lista->cont;

                lista->txt = stc_burbuja->txt;    // o palabra
                lista->cont =stc_burbuja->cont;  

                stc_burbuja->txt  = stc_tmp.txt;     
                stc_burbuja->cont = stc_tmp.cont;   
            }
            stc_burbuja = stc_burbuja->sig;
        }while(stc_burbuja != NULL);
        lista = lista->sig; 
    } 
    lista = stc_ini; // necesito colocar de nuevo el indice al principio
}

// Imprime un resumen estadístico
// -------------------------------------------------
void print_resumen(void){ 
    wchar_t pal_cade[10];
    wchar_t *strPal = L"Palabras";
    wchar_t *strCad = L"Cadenas";

    if (strings)
        wcscpy(pal_cade, strCad);
    else
        wcscpy(pal_cade, strPal);

    wprintf (L"\nTiempo proceso %u segundos\n", (end - start) );
    if (file_comp){
        wprintf (L"Num palabras No coincidentes con %s: %d\n", archivo_1, palabras_no_coincidentes); 
        wprintf (L"Num palabras Sí coincidentes con %s: %d\n", archivo_1, palabras_coincidentes); 
        wprintf (L"\nArchivo %s\n------------------\n", archivo_1); 
        wprintf (L"Caracteres: %d\n", caracteres_1); 
        wprintf (L"%ls leidas: %d\n", pal_cade, palabras_leidas_1); 
        wprintf (L"%ls distintas: %d\n", pal_cade, palabras_archivadas_1); 
        if (strings)
            wprintf (L"Emails: %d\n", emails_1); 
        wprintf (L"Caracter Nueva Línea: %d\n", lineas_1); 
        wprintf (L"Media longitud %ls: %2.2f\n",pal_cade, suma_1/palabras_archivadas_1); 
    }
    wprintf (L"\nArchivos\n------------------\n"); 
    wprintf (L"Caracteres: %d\n", caracteres); 
    wprintf (L"%ls leidas: %d\n", pal_cade, palabras_leidas); 
    wprintf (L"%ls distintas: %d\n", pal_cade, palabras_archivadas); 
    if (strings)
        wprintf (L"Emails: %d\n", emails); 
    wprintf (L"Caracter Nueva Línea: %d\n", lineas); 
    wprintf (L"Media longitud %ls: %2.2f\n",pal_cade, suma/palabras_archivadas);
}

// -------------------------------- Imprime instrucciones
void print_instruc(void){
    wprintf (L"%s",strayuda);
}
// ---------------------------- imprime ayuda
void print_version(void){
    wprintf (L"%s",strversion);
}

// partes locales de la dirección email.
// Ecluimos el punto porque no puede estar ni al principio ni 
// final ni doble. Lo procesamos a parte para ganar velocidad
// Los dígitos y caracteres también los procesamos en el parser
int islocalpart(wchar_t c){
    int result = 0;

    switch (c){
        
        case'!': case'#': case'$': case'%': case'&':
        case'\'': case'*': case'+': case'-': case'/':
        case'=': case'?': case'^': case'_': case'`':
        case'{': case'|': case'}': case'~': // case '.':
              result= 1;
    }
    return result;
}

//--------------------------------- caracter alfabético
int esalfabeto(wchar_t c){
   int result = 0;
  
   if ((c > 64 && c < 91) || (c > 96 && c < 123)) // A-Z, a-z 
           result = 1;

   return result;
}

// --------------- Guarda en archivo txt los emails y url 
int guardaemail(wchar_t *palabra){
    fwprintf(archemail, L"%ls\n", palabra);
            return CHACHI;
}
int guardaurl(wchar_t *palabra){
    fwprintf(archurl, L"%ls\n", palabra);
            return CHACHI;
}

// Buscará las palabras de lista en lista_1.
//---------------------------------------------------------------
int compara_listas(struct stc_pal *lista_1, struct stc_pal *lista){

struct stc_pal *lista_r;
palabras_coincidentes= 0;
palabras_no_coincidentes = 0;


    while(lista != NULL){
        lista_r = lista_1;
        while(lista_r != NULL){
            if(wcscmp(lista->txt, lista_r->txt) > 0 ){  // todavía no hemos llegado 
                lista_r = lista_r->sig; 
                continue;
            }else if(wcscmp(lista->txt, lista_r->txt) < 0 ) { // No está en lista_1
                if(listado && ! file_comp_yes)
                    wprintf(L"%ls\n", lista->txt);
                palabras_no_coincidentes++;
                break;
            }else{
                if(listado && file_comp_yes)
                    wprintf(L"%ls\n", lista->txt);
                palabras_coincidentes++;
                break; //palabra encontrada
            }
        }
        lista = lista->sig; 
    } 
    return 0;
}

// --------------------- Leemos las opciones de entrada
void lee_opciones(int argc, char **argv){
    int c;       // caracter
    int opt_indx = 0;
    
    while((c = getopt_long(argc, argv, "cdef:hln:rsuvV", opciones_largas, &opt_indx)) != -1)
        switch(c){
            case 'c': // por orden de conteo, descendiente
                conta = 1;
                break;
            case 'd':
                stad_word=0;    // por defecto imprime estadística de palabra
                break;
            case 'e':
                stad_summ=0;    // por defecto imprime estadística 
                break;
            case 'f':
                file_comp = 1;
                if (towupper(optarg[0]) == 'Y')
                    file_comp_yes =1;    // Buscar palabras coincidentes
                else if (towupper(optarg[0]) != 'N'){
                    wprintf(L"Error en los parámetros\n");
                    print_instruc();
                    exit(PARAM);
                }
                break;
            case 'h': 
                print_instruc();
                exit(0);
            case 'l':
                listado=0;
                break;
            case 'n':
                numax= atoi(optarg);
                break;
            case 'r':
                rever= 1;
                break;
            case 's':
                strings = 1;
                break;
            case 'u':
                urls= 1;
                break;
            case 'v':
                verbose= 1;
                break;
            case 'V':
                print_version();
                exit(0);
            default:
                wprintf(L"Error en los parámetros\n");
                print_instruc();
                exit(PARAM);
        }
}
