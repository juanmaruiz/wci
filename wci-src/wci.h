/* ------------------------------------------------------------------------------
// Programa wci Análisis estadístico de textos bajo licencia GPL V.3
//  Copyright (C) 2020 Juan Ruiz Segura juan@segura.pro

This program is free software: you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License 
along with this program. If not, see <https://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------*/ 

// wci.h
// ======================================================== 

#include <stdio.h>
#include <stdlib.h>
#include <wctype.h>
#include <locale.h>
#include <wchar.h>
#include <time.h>
#include <getopt.h>

# define MAXCAD 320
# define MAXPAL 32
# define MAXDNSLABEL 63
# define EMAILFILE "wci-url-email.txt"
# define URLFILE "wci-url.txt"

// ------------ Variables internas de control estado cadena
int long_palabra,
    puntos= 0,
    comas= 0,
    longdnslabel= 0,
    lastpoint = 0, // último caracter viso es punto
    lasthyphen = 0; // último caracter visto es un guión

enum estado_cadena {INICIO, PALABRA, NUMERO, CADENA, URL, LOCAL, EMAIL} ;
enum estado_cadena estado;
enum errores_tipo {CHACHI, VACIO, TRUNKPAL, TRUNKCAD, TRUNKDNSLABEL, PARAM, BADFILE, ERRMEM} error= CHACHI;

//- variables generales estadística de archivos
char *archivo_1;
int palabras_leidas=0,
    palabras_leidas_1=0,
    numeros_leidos = 0,
    claves_leidos = 0,
    palabras_archivadas=0,
    palabras_archivadas_1=0,
    caracteres,
    caracteres_1,
    lineas,
    lineas_1,
    emails, // contador de emails
    emails_1,
    lineas,
    lineas_1,
    palabras_coincidentes = 0,
    palabras_no_coincidentes = 0;


FILE *archemail; // Puntero al archivo de emails.
FILE *archurl; // Puntero al archivo de emails.

time_t start, end;
double suma,
       suma_1;

struct stc_pal{
    wchar_t *txt; 
    int cont;
    struct stc_pal *sig;
}; 
struct stc_pal *lista_ini,
               *lista_palab,
               *lista_palab_1; 


// parámetros de opciones
int conta=0,  // print por contador de palabra
    stad_word=1,  // print estad por defecto
    stad_summ=1,  // print estad por defecto
    file_comp = 0,   // compara archivos palabras por palabra. por defecto no coincidentes
    file_comp_yes = 0,   // compara archivos para palabras sí coincidentes
    listado=1,  // print listado por defecto
    urls=0,  // exporta los url y emails a un archivo txt.
    numax=0,  // número max de palabras a imprimir 0 = todas
    strings=0,  // Busca cadenas en lugar de palabras
    rever=0,  // orden reverso
    verbose=0;  // 
static struct option opciones_largas[] =
    {
        {"count", no_argument, 0, 'c'},
        {"stad-word-omit", no_argument, 0, 'd'},
        {"stad-summ-omit", no_argument, 0, 'e'},
        {"files-comp", required_argument, 0, 'f'},
        {"help", no_argument, 0, 'h'},
        {"list-omit", no_argument, 0, 'l'},
        {"number", required_argument, 0, 'n'},
        {"reverse", no_argument, 0, 'r'},
        {"string", no_argument, 0, 's'},
        {"url", no_argument, 0, 'u'},
        {"verbose", no_argument, 0, 'v'},
        {"version", no_argument, 0, 'V'},
        {0,0,0,0}
    };


// Declaración de funciones
// -------------------------------------------
int parser_cadenas(FILE *);
int parser_palabras(FILE *);
void reset_cadena();
int archiva_palabra(wchar_t *palabra, int long_palabra);
void print_lista_palab(struct stc_pal *lista);
int compara (int num_left, int num_right, int rev);
void ordena_lista_conteo(struct stc_pal *lista); 
void print_resumen(void); 
void print_instruc(void);
void print_version(void);
void imprime_resultados(void);
int compara_listas(struct stc_pal *lista_1, struct stc_pal *lista);
int islocalpart(wchar_t c);
int guardaemail(wchar_t *palabra);
int guardaurl(wchar_t *palabra);
int esalfabeto(wchar_t c);
struct stc_pal *guarda_pal_nueva(wchar_t *palabra, int lng_palabra);
void lee_opciones(int argc, char **argv);
 
char strversion[] = "wci v3.0.1 Juan Ruiz Segura juan@segura.pro\n";

char strayuda[] = "wci - Word Count Improved (v3.0.0) \n\n"
    "Por defecto, muestra un diccionario, por orden alfabético, de las palabras encontradas"
    " y una estadística\n\n"
    "Campos de Salida en cada palabra de diccionaro:\n"
    "Palabra, Longitud, Conteo, Probabilidad (separados por tabulación).\n"
    "Entrada por defecto STDIN\n" 
    "Salida por defecto STDOUT\n"
    "Muestra por defecto un listado alfabético (diccionario) de las \n" 
    "palabras del texto,en orden descendente y un resumen estadístico.\n\n" 
    "Modo de empleo: wci [OPCIÓN] [ARCHIVO]\n\n" "Opciones:\n" 
    " -c, --count\n"
    "       Imprime listado por orden de conteo de palabras en orden descendente,\n" 
    "    en lugar de por orden alfabético. Ver -r.\n\n" 
    " -d, --stad-word-omit\n"
    "       Omite la estadística de cada palabra. Por defecto la muestra.\n\n" 
    " -e, --stad-summ-omit\n"
    "       Omite la estadística final. Por defecto la muestra.\n\n" 
    " -f YES|NO, --files-comp YES|NO\n"
    "       Compara ARCHIVO1 con el resto de archivos que le siguen y \n"
    "       muestra listado alfabético de las palabras que SI|NO aparecen en ARCHIVO1.\n\n"
    " -h, --help\n"
    "       Imprime esta ayuda.\n\n"
    " -l, --list-omit\n"
    "       Omite el listado de palabras. Por defecto se imprime.\n\n" 
    " -n NUM, --number NUM \n"
    "       Número máximo de palabras a imprimir en orden descendiente.\n\n" 
    " -r, --reverese\n"
    "       Orden reverso para el conteo de palabras (usado junto con -c).\n\n"
    " -s, --string\n"
    "       Considera cadenas de caracteres imprimibles (strings) en lugar de palabras. Por defecto solo\n" 
    "       busca caracteres alfabéticos y diacrítios, para formar palabras de diccionarios.\n\n" 
    " -u, --url\n"
    "       Genera dos archivos de texto, uno con todos los emails ("EMAILFILE") encontrdos y otro\n"
    "       con todos los url potenciales ("URLFILE")encontrados. En caso de existir los archivos añade\n"
    "       nuevos registros a los ya existentes.\n\n"
    "-v, --verbose\n"
    "       Informa de cada palabra archivada.\n\n"
    "-V, --version\n"
    "       Muestra información del programa.\n\n"

    "Ejemplos:\n"
    "wci < archivo.txt  // Leerá de SDIN e imprimirá el listado alfabetico y resumen\n"
    "wci quijote.txt > dic.txt // Genera un diccionario de \"El Quijote\" y lo guarda en dic.txt.\n"
    "wci -n 20  archivo.txt   // Imprime 20 primeros de la lista alfabética\n"
    "wci -n 20 -c archivo.txt   // Imprime 20 primeros de la lista de conteo en orden descendente.\n"
    "wci -u archivo.txt    // Adicionalmente genera archivos con los url(potenciales) y email encontrados\n";
