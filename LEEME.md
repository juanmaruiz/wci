------------------------------------------------------------------------------
Programa wci Análisis estadístico de textos bajo licencia GPL V.3
Copyright (C) 2020 Juan Ruiz Segura juan@segura.pro

This program is free software: you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License 
along with this program. If not, see <https://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

wci - Word Count Improved
===========================================
Versión 3.0.2

Fecha de emisión 8-febrero-2021.

Autor: Juan Ruiz Segura: juan@segura.pro

Licencia bajo GPL V 3.0

Salida / Entrada
-----------------------------
- Por defecto, crea un diccionario de palabras y una estadística de un texto dado.
- Campos de Salida en cada palabra de diccionaro:
    Palabra, Longitud, Conteo, Probabilidad (separados por tabulación).
- Entrada por defecto STDIN 
- Salida por defecto STDOUT
- Muestra por defecto un listado alfabético (diccionario) de las 
    cadenas del texto,en orden descendente y un resumen estadístico.
- Longitud máxima de palabra 32 caracteres.
- Longitud máxima de cadena 320 caracteres.
    
Modo de empleo:
----------------
`wci [OPCIONES] [ARCHIVO1] [ARCHIVO2]...`

- El ejecutable (wci) y el archivo de texto a analizar deben estar en la misma carpeta.
- Si se ha descargado el ejecutable o compilado Vd., debe asegurarse de que tenga permisos de ejecución:
    En la carpeta del ejecutable introducir el comando:
    `juanma@mipc$ chmod a+x wci`


Opciones:
------------------------
-c, --count
       Imprime listado por orden de conteo de palabras en orden descendente,
    en lugar de por orden alfabético. Ver -r.

 -d, --stad-word-omit
       Omite la estadística de cada palabra. Por defecto la muestra.

 -e, --stad-summ-omit
       Omite la estadística final. Por defecto la muestra.

 -f YES|NO, --files-comp YES|NO
       Compara ARCHIVO1 con el resto de archivos que le siguen y 
       muestra listado alfabético de las palabras que SI|NO aparecen en ARCHIVO1.

 -h, --help
       Imprime esta ayuda.

 -l, --list-omit
       Omite el listado de palabras. Por defecto se imprime.

 -n NUM, --number NUM 
       Número máximo de palabras a imprimir en orden descendiente.

 -r, --reverese
       Orden reverso para el conteo de palabras (usado junto con -c).

 -s, --string
       Considera cadenas de caracteres imprimibles (strings) en lugar de palabras. Por defecto solo
       busca caracteres alfabéticos y diacrítios, para formar palabras de diccionarios.

 -u, --url
       Genera dos archivos de texto, uno con todos los emails (wci-url-email.txt) encontrdos y otro
       con todos los url potenciales (wci-url.txt)encontrados. En caso de existir los archivos añade
       nuevos registros a los ya existentes.

-v, --verbose
       Informa de cada palabra archivada.

-V, --version
       Muestra información del programa.

Utilidad
-------------------------------------
- Creación de diccionarios de palabras o cadenas imprimibles de un texto dado.

- Análisis estadístico de palabras y textos.

- Enseñanza Idiomas: Diseñé este programa para generar textos didácticos de idioma Español
  de forma graduada. Mi idea era preparar exámenes de idiomas para extrangeros y evitar
  examinar palabras que no ha estudado el alumno.

- Estudio linguístico de las diferntes lenguas. Ya existen varios pero son antiguos y este
  programa permite generar en segundos el trabajo de años de linguistas.

- Análisis criptográfico previo para generar diccionarios temáticos, obteniento 
  estadísticas según el tema del texto a analizar (médico, militar, poítico...).

- Creación de diccionarios tanto de palabras (opción -p) como de cadenas,
  claves y direcciones url/email.

- Extracción de claves potenciales y correos electrónicos de un gran texto y creación de 
    diccionarios de palabras.

 
Compilación
------------------------------------------
Archivo ejecutable compilado en Linux Debian 64 bits.

Para compilar, sólo se necesita incluir los archivos en la misma carpeta y desde allí ejecutar "make".

Si todo va bien, este creará el ejecutable wci.


Ejemplos:
----------------
`wci < archivo.txt`         Leerá archivo.txt desde STDIN y muestra listado alfabetico y resumen estadístico.\
`wci -n 20  archivo.txt`    Muestra 20 primeras cadenas, por orden alfabético, de la lista.\
`wci -n 20 -c archivo.txt`  Muestra 20 cadenas más frecuentesl del archivo.\
`wci -u archivo.txt`        Adicionalmente genera archivos con los url(potenciales) y email encontrados\
`wci -p quijote.txt > dic.txt` Genera un diccionario de "El Quijote" y lo guarda en dic.txt.\


Historial de versiones
------------------------------
- wci V 1.0.0 (2020-12-05) Juan Ruiz Segura juan@segura.pro/ Lanzamiento inicial.
- wci V 1.1.0 (2020-12-08) Juan Ruiz Segura juan@segura.pro/ Corrección errores conteo
- wci V 2.0.0 (2021-01-24) Juan Ruiz Segura juan@segura.pro/ Análisis de cadenas y url
- wci V 2.1.0 (2021-01-31) Juan Ruiz Segura juan@segura.pro/ Múltiples archivos, listados url-email.
- wci V 3.0.1 (2020-02-04)	
- wci V 3.0.0 (2021-02-04) Juan Ruiz Segura juan@segura.pro/ Compara archivos buscando palabras no coincidentes en el primer archivo.
- wci V 3.0.1 (2021-02-04) Juan Ruiz Segura juan@segura.pro/ Opción omitir estadística de cada palabra.
- wci V 3.0.2 (2021-02-08) Juan Ruiz Segura juan@segura.pro/ Opciones largas GNU

     
